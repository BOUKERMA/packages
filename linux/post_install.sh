#!/bin/sh

echo "installing java" > /tmp/post_install.log
date >> /tmp/post_install.log
yum install -y java-1.8.0-openjdk
echo "java installed ?" >> /tmp/post_install.log
date >> /tmp/post_install.log
